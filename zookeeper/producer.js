import { Kafka } from "kafkajs";
const topicName = "";
console.log("Create new kafka client with broker");
// Create the client with the broker list
const kafka = new Kafka({
  clientId: "kafla-client",
  brokers: ["127.0.0.1:9092"], //Don't use 'localhost'
});

console.log("Create new admin");
const admin = kafka.admin();
await admin.connect();
console.log("Get topics");
const topics = await admin.listTopics();
console.log(`Check if ${topicName} exists`);
console.log(topics);
if (!topics.includes(topicName)) {
  console.log(
    `Creation Topic ${topicName}...`);
  const isCreated = await admin.createTopics({
    topics: [{ topic: topicName }],
    waitForLeaders: true // it will wait until metadata for the new topics doesn't throw LEADER_NOT_AVAILABLE
  });
  console.log(
    `Topic ${topicName} is ${isCreated ? "created" : "not created"} !`
  );
}

console.log("Create new producer");
const producer = kafka.producer();
await producer.connect();
console.log("Send messages in topic-1");
await producer.send({
  topic: topicName,
  messages: [
    { key: "key1", value: "hello world" },
    { key: "key2", value: "hey hey!" },
    { key: "key3", value: "John Doe" },
    { key: "key4", value: "Evens Pompe" },
    { key: "key5", value: "Kafka" },
  ],
});
console.log("done");
process.exit(0);
