import { Kafka } from "kafkajs";
const topicName = "";
console.log("Create new kafka client with broker");
// Create the client with the broker list
const kafka = new Kafka({
  clientId: "kafla-client",
  brokers: ["127.0.0.1:9092"], // Don't use 'localhost'
});
console.log("Create new consumer");
const consumer = kafka.consumer({ groupId: "consumers-group" });
await consumer.connect();
console.log(`Consumer subscribe ${topicName}`);
await consumer.subscribe({ topics: [topicName], fromBeginning: true });
await consumer.run({
  eachMessage: async ({ topic, partition, message, heartbeat, pause }) => {
    console.log({
      key: message.key.toString(),
      value: message.value.toString(),
      headers: message.headers,
    });
  },
});
await new Promise((resolve) => setTimeout(resolve, 6000));

await consumer.disconnect();
console.log("done");
