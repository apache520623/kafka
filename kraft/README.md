# Kafka with KRAFT

## Getting started

### Requirements in local

- Docker or Podman (with Compose) installed
- NVM with nodejs 18.x or further

### Instruction

Ensure to turn on Docker before

Firstly, execute the command :

```bash
docker compose up -d
```

or

```bash
podman compose up -detach
```

Then, go to the script `producer.js` and `consumer.js` to add your topic's name in `topicName` variable.

After that, run producer script to create your topic if not exist, and send messages :

```bash
node producer.js
```

Finally, run consumer script to display the messages stored in your topic :

```bash
node consumer.js
```
